from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def index():
    dolar = request.args.get("dolar", "")
    if dolar:
        valorFinal = dolar_from(dolar)
    else:
        valorFinal = ""

    return (
        	"""<h2>Converção de Dolar para Real</h2>"""
		"""<br>"""
		"""<form action="" method="get">
                <input type="text" name="dolar">
                <input type="submit" value="Converter">
            </form>"""
        + "Valor em reais: "
        + '<a id="dolar">' +valorFinal+ '</a>'

    )
 
@app.route("/<float:dolar>")
def dolar_from(dolar):
    """Converter Dolar para Real."""
    dolar = float(dolar) * 5.5    
    return str(dolar)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
     #app.run(host="0.0.0.0", port=8080, debug=False)
